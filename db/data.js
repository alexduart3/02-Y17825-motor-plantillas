// import { imagen-personalidad }

exports.integrantes = [
    { nombre: 'Alexis Adrián', apellido: 'Duarte Flecha', matricula: 'Y17825' },
    { nombre: 'Lennys Monserrth', apellido: 'Cantero Franco', matricula: 'Y26426' },
    { nombre: 'Gabriel', apellido: 'Garcete', matricula: 'Y23865' },
    { nombre: 'Elvio', apellido: 'Martinez', matricula: 'Y12858' },
    { nombre: 'Marco', apellido: 'Ortigoza Colman', matricula: 'UG0507' }
]

exports.tipoMedia = [
    { nombre: 'Imagen' },
    { nombre: 'Youtube' },
    { nombre: 'Dibujo' }
]

exports.media = [
    { tipoMedia: 'Imagen', url: null, src: '/assets/images/imagen-personalidad.jpg', matricula:'Y17825', titulo: 'Imagen favorita', alt: 'Imagen de un Golden que me representa' },
    { tipoMedia: 'Youtube', src: null, url: 'https://www.youtube.com/embed/U1ivmi3_IeI?si=9QjM1bJzS3uUpMYk', matricula:'Y17825', titulo: 'Video favorito de YouTube' },
    { tipoMedia: 'Dibujo', url: null, src: '/assets/images/dibujo-sistema-solar.png', matricula:'Y17825', titulo: 'Dibujo favorito', alt: 'Mi dibujo hecho en paint' },

    { tipoMedia: 'Imagen', url: null, src: '/assets/images/saturno.jpg', matricula:'UG0507', titulo: 'Imagen favorita', alt: 'Imagen de un Golden que me representa' },
    { tipoMedia: 'Youtube', src: null, url: 'https://www.youtube.com/embed/RW75cGvO5xY', matricula:'UG0507', titulo: 'Video favorito de YouTube' },
    { tipoMedia: 'Dibujo', url: null, src: '/assets/images/TERERE.png', matricula:'UG0507', titulo: 'Dibujo favorito', alt: 'Mi dibujo hecho en paint' },

    { tipoMedia: 'Imagen', url: null, src: '/assets/images/melissa.jpg', matricula:'Y12858', titulo: 'Imagen favorita', alt: 'Imagen de un Golden que me representa' },
    { tipoMedia: 'Youtube', src: null, url: 'https://www.youtube.com/embed/VhoHnKuf-HI', matricula:'Y12858', titulo: 'Video favorito de YouTube' },
    { tipoMedia: 'Dibujo', url: null, src: '/assets/images/dibujo-Elvio.png', matricula:'Y12858', titulo: 'Dibujo favorito', alt: 'Mi dibujo hecho en paint' },

    { tipoMedia: 'Imagen', url: null, src: '/assets/images/messi_pou.jpeg', matricula:'Y23865', titulo: 'Imagen favorita', alt: 'Imagen de un Golden que me representa' },
    { tipoMedia: 'Youtube', src: null, url: 'https://www.youtube.com/embed/B4LvDiIi128?rel=0', matricula:'Y23865', titulo: 'Video favorito de YouTube' },
    { tipoMedia: 'Dibujo', url: null, src: '/assets/images/paint_garcete.jpg', matricula:'Y23865', titulo: 'Dibujo favorito', alt: 'Mi dibujo hecho en paint' },

    { tipoMedia: 'Imagen', url: null, src: '/assets/images/imagen-56.jpeg', matricula:'Y26426', titulo: 'Imagen favorita', alt: 'Imagen de un Golden que me representa' },
    { tipoMedia: 'Youtube', src: null, url: 'https://www.youtube.com/embed/vi6-oF3tsPs?si=AdywmEQJk-ewbUQg', matricula:'Y26426', titulo: 'Video favorito de YouTube' },
    { tipoMedia: 'Dibujo', url: null, src: '/assets/images/imagen-57.png', matricula:'Y26426', titulo: 'Dibujo favorito', alt: 'Mi dibujo hecho en paint' },
]

exports.home = [
    { nombre: 'FSOCIETY', titulo: 'Bienvenidos al grupo', src: '/assets/images/logo.jpeg', alt: 'Grupo FSOCIETY' }
]
