const express = require("express");
const path = require("path");
const {request, response} = require("express");
const bd = require("./db/data");
const hbs = require("hbs");
const {media} = require("./db/data");

const app = express();

// Servir archivos estáticos desde la carpeta "public"
app.use(express.static(__dirname + "/public"));
app.set("view engine", "hbs");
app.set("views", __dirname + "/views");

hbs.registerPartials(__dirname + "/views/partials");

// Ruta localhost:{puerto}
app.get("/", (request, response) => {
    response.render("index", {
        home: bd.home[0]
    });
});

app.get("/word_cloud/", (request, response) => {
    response.render("word_cloud");
});

app.get("/curso/", (request, response) => {
    response.render("curso");
});

const matriculas = [...new Set(bd.media.map((item) => item.matricula))];

app.get("/:matricula", (request, response, next) => {
    const matricula = request.params.matricula;
    if (matriculas.includes(matricula)) {
        const integranteFilter = bd.integrantes.filter((integrante) => integrante.matricula === matricula);
        const mediaFilter = bd.media.filter((media) => media.matricula === matricula);
        response.render('integrante', {
            integrante: integranteFilter,
            tipoMedia: bd.tipoMedia,
            media: mediaFilter
        });
    } else {
        next();
    }
});

app.use((req, res) => {
    res.status(404).render('page_404');
});

// Iniciar el servidor en el puerto 3000
app.listen(3000, () => {
    console.log("Servidor corriendo en el puerto 3000");
    console.log("http://localhost:3000/");
});

// console.log('Base de Datos estática: ', bd);
